import Vue from 'vue'
import Router from 'vue-router'
import axios from 'axios'
import Home from './views/Home.vue'
import Header from './components/Header.vue'
import Akkinf from './components/Akkinf.vue'
import Akklist from './components/Akklist.vue'
import Autoriz from './components/Autoriz.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      children: [
        {
          path: '/Header',
          name: 'Header',
          component: Header

        },
        {
          path: '/Akklist',
          name: 'Akklist',
          component: Akklist
        },
        {
          path: '/Akkinf:id',
          name: 'Akkinf',
          component: Akkinf
        }
      ]
    },
  ]
})